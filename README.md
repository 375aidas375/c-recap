# Programos naudijimas

Programa visas funkcijas atlieka su programoje apibrestais tekstais,
Visi rezultatai isvedami i terminala

## Programos paleidimas

Windowsams naudoti komanda:

```
gcc Main.c -o Main && Main
```

Linux naudoti komanda:

```
gcc Main.c -o Main && ./Main
```
