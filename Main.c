#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define DEFAULT_BUFLEN 48

void move(){
    int *cpy, *mov; 
    char mcpy [DEFAULT_BUFLEN];
    char mmove [DEFAULT_BUFLEN];
    char start [] = "Memory movement";
    int txts = strlen(start);

    printf("Dirbant su mem komandom patartina isvalyti atminties bloka\n");

    memset(mcpy,0,DEFAULT_BUFLEN);
    memset(mmove,0,DEFAULT_BUFLEN);

    memmove(mmove, start, txts);
    memcpy(mcpy, start, txts);

    printf("Pranesimui -%s- kurio ilgis %d perkelti naudojam memcopy ir memmove\nRezultatai:\nmemcpy: %s\nmemmove: %s\n", start, txts, mcpy, mmove);
    printf("Is gauto rezultato matome kad memcpy ir memmove rezultata duoda tapati, taciau jei atminties blokai persidengia saugiau naudoti yra memmove\n\n");

    char scpy [DEFAULT_BUFLEN];
    char sncpy [DEFAULT_BUFLEN];
    
    memset(scpy,0,DEFAULT_BUFLEN);
    memset(sncpy,0,DEFAULT_BUFLEN);

    strcpy(scpy, "Parasytas tekstas");
    strncpy(sncpy, scpy, 5);

    printf("str komandos nuo mem skiriasi tuo, kad str atlieka darbus su tekstu esanciu bloke, o mem su atminties blokais\n");
    printf("strcpy galima naudoti netik teksto kopijavimui bet ir ikeliti teksto masyva, o strncpy galima nuroditi kiek simboliu perkelti\n");
    printf("strcpy: %s\nstrncpy: %s\n\n", scpy, sncpy);

    char scat [DEFAULT_BUFLEN];
    char sncat [DEFAULT_BUFLEN];

    memset(scat,0,DEFAULT_BUFLEN);
    memset(sncat,0,DEFAULT_BUFLEN);

    strcpy(scat, "Pirmas tekstas - ");
    strcpy(sncat, "Pirmas tekstas - ");
    
    printf("strcat ir strncat yra naudojami prideti teksta prie esamo, strncat galima nuroditi kiek simboliu prideti\nPrie sukurto teksto pridedam pati pirmajy teksta, antruoju variantu pridedam tik 5 simbolius\n");
    printf("strcat: %s\n", scat);
    
    strncat(sncat, start, 5);
    
    printf("strncat: %s\n\n", sncat);
    
}

void comp(){
    char text1[DEFAULT_BUFLEN];
    char text2[DEFAULT_BUFLEN];
    char text3[DEFAULT_BUFLEN];

    memcpy(text1,"abcdefg",9);
    memcpy(text2,"abcd",5);
    memcpy(text3,"ABCdefg",9);

    printf("text1: %s\ntext2: %s\ntext3: %s\n\n", text1, text2, text3);

    printf("Compere funkcijose rezultatai yra pateikiami 3 budais:\nres > 0 - text2 mazesnis uz text1\nres < 0 - text1 mazesnis uz text2\nres = 0 - text1 lygus text2\n\n");

    int res;
    res = memcmp(text1, text2, 4);

    printf("Naudodami memcmp galime pasirinkti kokio diddzio atminties bloka tikrinti\nsiuo atveju tikrinam pirmo ir antro texto blokus taciau imant tik 4 simbolius ir rezultasa: %d\n\n", res);

    res = strcmp(text1, text2);

    printf("Naudojant strcmp simboliu masyvo didzio nuroditi nereikia, tikrina visa masyva\ntikrinant pirma ir antra masyvus gauname toki rezultata: %d\n\n", res);
    
    res = strcoll(text1, text3);

    printf("Naudojant strcoll tekstai yra tikrinami pagal 'LC_COLLATE' nustatyta seka\ntikrinant pirma ir trecia masyvus rezultatas: %d\n\n", res);
        
    res = strncmp(text1, text3, 4);

    printf("Naudojant strncmp nurodome kiek simboliu tikrinti is pasirinktu masyvu\ntikrinant pirma ir trecia masyva 4 kintamuju rezultatas: %d\n\n", res);
}

void find(){
    char text[DEFAULT_BUFLEN];
    char chr;
    char *pl;
    int siz = 11;

    memset(text,0,DEFAULT_BUFLEN);
    memcpy(text,"Automobilis",siz);
    printf("Simbolio radimui naudosime texta '%s', jeigu rezultate gauname null, vadinas tokio simbolio nera\n\n", text);

    chr = 'o';
    pl = memchr(text, chr, siz);

    printf("Naudojant memchr randame pointer(adreso vieta) i pirmaji atitinkama simboli ir irasom i nauja masyva, tam papildomai reikia nuroditi kokio didzio atmintyje ieskoti\nGauname nuo simbolio %c zodi %s\n\n", chr, pl);

    chr = 'm';
    pl = strchr(text, chr);

    printf("Naudojant strchr nereik nurodit masivo didzio, tikrina visa. funkcija taipogi grazina pointeri i pirma toki pat simboli\nGauname nuo simbolio %c zodi %s\n\n", chr, pl);

    char chk[DEFAULT_BUFLEN];
    int res;
    memset(chk, 0, DEFAULT_BUFLEN);
    memcpy(chk, "lim", 3);

    res = strcspn(text, chk);
    printf("strcpn grazina sveika skaiciu, kuris parodo kiek kintamuju masyvo pradzioje nepriklauso antrajam masyvui: %d\n", res);
    printf("Tikrinamasis: %s - pagal ka tikrino: %s\n\n", text, chk);
    

}

void sear(){
    char text[] = "asdxcgdarhiusgdf4adfgXZ3";
    char need[] = "34";
    char *res;

    res = strpbrk(text, need);
    printf("Naudojamas tekstas: %s\nIeskomi simboliai %s\n", text, need);
    printf("strpbrk komanda naudojama rasti simboli esanciame masyve ir grazina simbolio adresa\nPirmas rastas simbolis: %c\n\n", *res);    

    char ssh = 'g';
    res = strrchr(text, ssh);
    printf("Strrchr komanda naudojama rasti paskutini simboli (%c) esanti masyve, ir grazina simbolio adresa\nTekstas einantis nuo rasto simbolio: %s\n\n", ssh, res);

    char nee[] = "gd";
    res = strstr(text, nee);
    printf("Strstr komanda naudojama rasti texta texte (%s), ir grazina adresa i pirma pasitaikiusi vienoda teksta: %s\n\n", nee, res);

    printf("Strtok naudojama atskirti teksta pagal pateikta simboli, pateikus kelis kaip (%s) teksta skelia ir pagal ta ir pagal kita simboli\nAtskirtas tekstas:\n", nee);
    res = strtok(text, nee);
    while( res != NULL ) {
        printf( " %s\n", res );
        res = strtok(NULL, nee);
    }
    
}

void ra(){
    char text [DEFAULT_BUFLEN];
    printf("Memset naudojama nustatyti atminties blokui pasirinktais simboliais\nTaciau praktiska pries naudojant kokias nors mem funkcijas atminties bloka isivalyti (nustatyti 0)\n");
    printf("1: kas buvo uzsilike atminties bloke pries paleidziant programa\n2: atminties blokas nustatytas pasirinktu simboliu\n");
    printf("ISPEJIMAS: kai naudojame memset nustatyti kaskokiu kitokiu simboliu bloka paskutinis simbolis turi buti 0 arba kitaip gausite 'overflow'\n\n");
    printf("1: %s\n", text);
    memset(text, 0, DEFAULT_BUFLEN);
    memset(text, 'c', DEFAULT_BUFLEN-1);
    printf("2: %s\n\n", text);

    printf("Strlen komanda grazina naudojamo masyvo dydi\nKaip pirmai naudoto masyvo naudojamas dydis: %ld; o jo maksimalus deklaruotas dydis: %d\n\n", strlen(text), DEFAULT_BUFLEN);

    printf("Printf funkcija naudojama tekstui isvesti i terminala\n\n");

    FILE *fp;
    fp = fopen("file.txt", "w+");
    fprintf(fp, "%s", text);
    fclose(fp);

    printf("Fprintf naudojame irasyti norimam tekstui i faila, kaip dabar sukurtas textinis filas 'file.txt' ir jame yrasytas priestai naudotas masyvas\n\n");

    char new[100];
    char dienis[20], menesis[20];
    int diena, metai;
    
    strcpy(new, "Penktadienis Gruodzio 25 2020");
    sscanf(new, "%s %s %d %d", dienis, menesis, &diena, &metai);

    printf("Sscanf funkcija esama masyva nuskaito apibrestu formatu\nVisas masyvas: %s\n", new);
    printf("Data: %s %d %d\nDiena: %s\n\n", menesis, diena, metai, dienis);

}

void num(){
    char numbers[DEFAULT_BUFLEN];
    float val1;
    int val2;
    long val3;

    strcpy(numbers, "954215413");

    val1 = atof(numbers);
    val2 = atoi(numbers);
    val3 = atol(numbers);

    printf("Tektstas kuri kopijuosma i skaicius: %s\n", numbers);
    printf("atof funkcija is tekstinio masyvo padaro float tipo skaicius: %f\n", val1);
    printf("atoi funkcija is tekstinio masyvo padaro intiger tipo skaiciu: %d\n", val2);
    printf("atol funkcija is tekstinio masyvo padaro long tipo skaiciu: %ld\n", val3);

    strcpy(numbers, "kaskas001452");
    val2 = atoi(numbers);

    printf("Taciau jei masyve bus daugiau nei skaiciai rezultatas bus: %d\n\n", val2);
}

void chek(){
    int var[6] = {'4', 'h', ' ', '9', 'D', '@'};

    printf("Isdigit naudojamas surasti ar kintamasis yra skaicius:\n");
    for (int i = 0; i < 6; i++)
    {
        if (isdigit(var[i]))
        {
            printf("Kintamasis[%d]: yra skaicius - '%c'\n", i, var[i]);
        }else{
            printf("Kintamasis[%d]: nera skaicius - '%c'\n", i, var[i]);
        }    
    }

    printf("\nIsalnum naudojamas surasti ar kintamasis yra alfanumerinis:\n");
    for (int i = 0; i < 6; i++)
    {
        if (isalnum(var[i]))
        {
            printf("Kintamasis[%d]: yra alfanumerinis - '%c'\n", i, var[i]);
        }else{
            printf("Kintamasis[%d]: nera alfanumerinis - '%c'\n", i, var[i]);
        }    
    }
    
    printf("\nIsalpha naudojamas surasti ar kintamasis yra raide:\n");
    for (int i = 0; i < 6; i++)
    {
        if (isalpha(var[i]))
        {
            printf("Kintamasis[%d]: yra raide - '%c'\n", i, var[i]);
        }else{
            printf("Kintamasis[%d]: nera raide - '%c'\n", i, var[i]);
        }    
    }

    printf("\nIsupper naudojamas surasti ar kintamasis yra didzioji raide:\n");
    for (int i = 0; i < 6; i++)
    {
        if (isupper(var[i]))
        {
            printf("Kintamasis[%d]: yra didzioji raide - '%c'\n", i, var[i]);
        }else{
            printf("Kintamasis[%d]: nera didzioji raide - '%c'\n", i, var[i]);
        }    
    }
}

void main(){
    move();
    printf("\n------------------------------------------------------------------------------------\n\n");
    comp();
    printf("\n------------------------------------------------------------------------------------\n\n");
    find();
    printf("\n------------------------------------------------------------------------------------\n\n");
    sear();
    printf("\n------------------------------------------------------------------------------------\n\n");
    ra();
    printf("\n------------------------------------------------------------------------------------\n\n");
    num();
    printf("\n------------------------------------------------------------------------------------\n\n");
    chek();
    printf("\n------------------------------------------------------------------------------------\n\n");
}
